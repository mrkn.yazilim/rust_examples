
use std::io;

fn main() {
    let mut user_input = String::new();
    let stdin = io::stdin(); 
    println!("Please enter number ");
    match stdin.read_line(&mut user_input) {
        Ok(_) => {
            let user_number: i32 = user_input.trim().parse().expect("Please give me correct string number!");
            let mut counter = 2;
            if user_number < 100 {
                println!("Show all prime numbers");
                println!("-----------------------------------------------------");
                loop {
                    if counter > user_number {
                        break ;
                    }
                    let mut tmp_counter = 2;
                    let is_prime = loop {
                         if counter % tmp_counter == 0 && counter == tmp_counter {
                            break true; 
                         } 
                         else if counter % tmp_counter == 0 || tmp_counter > counter {
                            break false; 
                         }
                         tmp_counter +=1;
                    };
                   if is_prime {
                    println!("{:0>3} is prime number", counter)
                   }
                   counter +=1;
                };
                println!("-----------------------------------------------------");
            }
            else {
                println!("Given number({})  must be lower then 100 ",user_number)
            }
        }
            
        Err(error) => println!("error: {error}"),
    }   


   
}