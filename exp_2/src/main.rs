use rand::Rng;

fn main() {
    let num = rand::thread_rng().gen_range(0..100);
    println!("Rastgele Sayı : {}", num);
    if num % 2 == 0 {
        println!("Sayı ikiye bölünebiliyor: {}", num);
    }
    else if num % 3 == 0 {
        println!("Sayı üçe bölünebiliyor: {}", num);
    }
    else{
        println!("Sayı Altıya Bölünemez: {}", num);
    }
}
